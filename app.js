//const name = require('./utils.js');
//const fs = require('fs');
const validator = require('validator');
const chalk = require('chalk');
const yargs = require('yargs');


//fs.writeFileSync('notes.txt', 'this is for again practice purpose');
//fs.writeFileSync('notes.txt', 'This file was created by node.js');
//fs.appendFileSync('notes.txt', ". My name is Shiam.");

const notes = require('./notes.js');
const { argv } = require('yargs');

// const mynote = getNote();

// console.log(mynote);

// console.log(validator.isEmail('shiam@gmail.com'));
// console.log('url check: ' + validator.isURL('https://asdasd.com'));

// console.log(chalk.blue("thinking!"));

yargs.command({
    command: "add",
    describe: "To add a new note",
    builder:{
        title: {
            describe: "Note title",
            demandOption: true,
            type: 'string'
        },
        body: {
            describe: "Note description",
            demandOption: true,
            type: 'string'
        }
    },
    handler(argv){
        //console.log("adding a new note!", argv);
        // console.log("Title: " + argv.title);
        // console.log("Description: " + argv.body);

        notes.addNote(argv.title, argv.body)
    }
})

yargs.command({
    command: "remove",
    describe: "To remove a note",
    builder:{
        title: {
            describe: "note title",
            demandOption: true,
            type: 'string'
        }
    },
    handler(argv){
        notes.removeNote(argv.title)
    }
})

yargs.command({
    command: "list",
    describe: "To list a note",
    handler(){
        notes.listNotes()
        //console.log("listing a note");
    }
})

yargs.command({
    command: "read",
    describe: "To read a note",
    builder:{
        title:{
            describe: "read note",
            demandOption: true,
            type: 'string'
            
        }
    },
    handler(argv){
        notes.readNote(argv.title)
        //console.log("reading a note!");
        
    }
})

//console.log(yargs.argv);

yargs.parse();