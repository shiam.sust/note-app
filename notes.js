const fs = require('fs')
const chalk = require('chalk')


const addNote = (title, body) => {
    const notes = loadNode()
    // console.log(notes)

    //const duplicateNotes = notes.filter((note) => note.title === title )
    const duplicateNote = notes.find((note) => note.title === title )

    if(!duplicateNote){
        notes.push({
            title: title,
            body: body
        })
        saveNotes(notes)
        console.log(chalk.bgGreen("new node added!"));
    }else{
        console.log(chalk.bgRed("note title already taken!"));
    }
    
}

const removeNote = (title) => {
    console.log("title to be removed: " + title)
    const notes = loadNode()

    const newArray = notes.filter((note) => note.title !== title )

    if(notes.length !== newArray.length){
        saveNotes(newArray)
        console.log(chalk.bgGreen("note removed!"));

    }else{
        console.log(chalk.bgRed("note not found!"));
    }
}

const listNotes = () => {
    const notes = loadNode();

    if(notes.length >= 0){
        console.log(chalk.blue("Your Notes: "));
        notes.forEach((note) => console.log(note.title));
    }else{
        console.log(chalk.red("No notes found!"));
    }
}

const readNote = (title) => {
    const notes = loadNode()

    const note = notes.find((note) => {
        return note.title === title
    })

    if(note){
        console.log(chalk.green(note.title) + ": " + note.body);
    }else{
        console.log(chalk.red("No such note found!"))
    }
}

const saveNotes = (notes) => {
    const stringData = JSON.stringify(notes)
    fs.writeFileSync('notes.json', stringData)
}

const loadNode = () => {
    try{
        const dataBuffer = fs.readFileSync('notes.json')
        const dataJSON = dataBuffer.toString()
        return JSON.parse(dataJSON)
    } catch(e){
        return []
    }
    
}

module.exports = {
    addNote: addNote,
    removeNote: removeNote,
    listNotes: listNotes,
    readNote: readNote
}
